<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 08/06/18
 * Time: 11:44
 */

namespace App\Controller;

use App\common\manager\CommentEntityManager;
use App\common\manager\TrackEntityManager;
use App\common\response\ResponseCode;
use App\common\response\ResponseError;
use App\common\response\ResponseMessage;
use App\common\response\ResponseObject;
use App\common\validator\ValidatorException;
use App\common\validator\ValidatorManager;
use App\Entity\Comment;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class CommentController extends BaseWSController {

    /**
     * @Route("/comment/{id}", name="ws_get_comment", methods="GET")
     */
    public function getAction($id) {
        $entityManager = new CommentEntityManager($this->getDoctrineManager());
        $comment = $entityManager->get($id);
        $response = new ResponseObject();

        if($comment == null) {
            $response->withError(new ResponseError(ResponseCode::NOT_FOUND, ResponseMessage::COMMENT_NOT_FOUND));

            return $this->renderResult($response);
        }

        $response->withParams('comment', $comment->toArray());

        return $this->renderResult($response);
    }

    /**
     * @Route("/comment", name="ws_post_comment", methods="POST")
     */
    public function postAction(Request $request){
        $params = $this->getRequestParams($request);
        $response = new ResponseObject();
        $manager = new CommentEntityManager($this->getDoctrineManager());

        $comment = new Comment();
        $comment->buildFromRequest($params);

        try {
            $notValidatedFields = ValidatorManager::validate($comment);
        } catch (ValidatorException $e) {
            $response->withError(new ResponseError(ResponseCode::SERVER_ERROR, ResponseMessage::SERVER_ERROR));

            return $this->renderResult($response);
        }

        if(!empty($notValidatedFields)) {
            $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::BAD_REQUEST));
            $response->withParams('fields', $notValidatedFields);

            return $this->renderResult($response);
        } else {
            $createdComment = $manager->create($comment);

            $response->withCode(ResponseCode::CREATED);
            $response->withParams('comment', $createdComment->toArray());
        }

        return $this->renderResult($response);

    }

    /**
     * @Route("/comment/{id}", name="ws_put_comment", methods="PUT")
     */
    public function putAction($id, Request $request) {
        $params = $this->getRequestParams($request);
        $entityManager = new CommentEntityManager($this->getDoctrineManager());
        $comment = $entityManager->get($id);
        $response = new ResponseObject();

        if($comment == null) {
            $response->withError(new ResponseError(ResponseCode::NOT_FOUND, ResponseMessage::COMMENT_NOT_FOUND));

            return $this->renderResult($response);
        }

        try {
            $comment->updateWithParams($params);
        } catch (ParameterNotFoundException $e) {
            $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::INVALID_PARAMETER . $e->getKey()));

            return $this->renderResult($response);
        }

        try {
            $notValidatedFields = ValidatorManager::validate($comment);
        } catch (ValidatorException $e) {
            $response->withError(new ResponseError(ResponseCode::SERVER_ERROR, ResponseMessage::SERVER_ERROR));

            return $this->renderResult($response);
        }

        if(!empty($notValidatedFields)) {
            $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::BAD_REQUEST));
            $response->withParams('fields', $notValidatedFields);

            return $this->renderResult($response);
        } else {
            try {
                $createdComment = $entityManager->update($id, $comment);
            } catch(\Exception $e) {
                $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::BAD_REQUEST));
                $response->withParams('fields', $notValidatedFields);

                return $this->renderResult($response);
            }

            $response->withCode(ResponseCode::CREATED);
            $response->withParams('comment', $createdComment->toArray());
        }

        return $this->renderResult($response);
    }

        /**
     * @Route("/comment/{id}", name="ws_delete_comment", methods="DELETE")
     */
    public function deleteAction($id) {
        $entityManager = new TrackEntityManager($this->getDoctrineManager());
        $comment = $entityManager->get($id);
        $response = new ResponseObject();

        if($comment == null) {
            $response->withError(new ResponseError(ResponseCode::NOT_FOUND, ResponseMessage::COMMENT_NOT_FOUND));

            return $this->renderResult($response);
        }

        $entityManager->delete($id);
        $response->withCode(ResponseCode::NO_CONTENT);

        return $this->renderResult($response);
    }

}