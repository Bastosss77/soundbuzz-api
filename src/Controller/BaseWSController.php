<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 04/06/18
 * Time: 14:56
 */


namespace App\Controller;

use App\common\response\ResponseMessage;
use App\Services\Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use App\common\response\ResponseObject;
use App\common\response\ResponseError;
use App\common\response\ResponseCode;

abstract class BaseWSController extends Controller {
    private const HEADER_CONTENT_TYPE_KEY = 'Content-type';
    private const HEADER_CONTENT_TYPE_VALUE = 'text/json';
    private const HEADER_CHARSET_VALUE = 'UTF-8';
    private const HEADER_CROSS_ORIGIN_KEY = 'Access-Control-Allow-Origin';
    private const HEADER_CROSS_ORIGIN_VALUE = '*';
    private const HEADER_ALLOW_METHODS_KEY = 'Access-Control-Allow-Methods';
    private const HEADER_ALLOW_METHODS_VALUE = 'POST, GET, PUT, DELETE, OPTIONS';

    public function renderResult(ResponseObject $responseObject) {
        $response = new Response($responseObject->toJson());
        $response->setStatusCode($responseObject->getCode());
        $response->headers->set(BaseWSController::HEADER_CONTENT_TYPE_KEY, BaseWSController::HEADER_CONTENT_TYPE_VALUE);
        $response->headers->set(BaseWSController::HEADER_CROSS_ORIGIN_KEY, BaseWSController::HEADER_CROSS_ORIGIN_VALUE);
        $response->headers->set(BaseWSController::HEADER_ALLOW_METHODS_KEY, BaseWSController::HEADER_ALLOW_METHODS_VALUE);
        $response->setCharset(BaseWSController::HEADER_CHARSET_VALUE);

        return $response;
    }

    protected function getDoctrineManager() {
        return $this->getDoctrine()->getManager();
    }

    protected function getRequestParams(Request $request) {
        return $request->request->all()['form'] ?? $request->request->all();
    }

    protected function getQueryParams(Request $request) {
        return $request->query->all();
    }

    private function renderNotImplemented() {
        $response = new ResponseObject();
        $response->withError(new ResponseError(ResponseCode::NOT_IMPLEMENTED, ResponseMessage::ENDPOINT_NOT_IMPLEMENTED));
        return $this->renderResult($response);
    }


    protected function getAction($id) {
        return $this->renderNotImplemented();
    }

    protected function postAction(Request $request) {
        return $this->renderNotImplemented();
    }

    protected function putAction($id, Request $request) {
        return $this->renderNotImplemented();
    }

    protected function deleteAction($id) {
        return $this->renderNotImplemented();
    }
}