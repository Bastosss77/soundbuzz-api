<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 04/06/18
 * Time: 16:02
 */

namespace App\Controller;


use App\common\exception\InvalidParameterException;
use App\common\manager\UserEntityManager;
use App\common\response\ResponseCode;
use App\common\response\ResponseError;
use App\common\response\ResponseMessage;
use App\common\response\ResponseObject;
use App\common\utils\CryptUtils;
use App\common\utils\ServiceUtils;
use App\common\utils\TypeUtils;
use App\common\validator\ValidatorException;
use App\common\validator\ValidatorManager;
use App\Entity\User;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends BaseWSController {

    /**
     * @Route("/user/{id}", name="ws_get_user", methods="GET")
     */
    public function getAction($id) {
        $entityManager = new UserEntityManager($this->getDoctrineManager());
        $user = $entityManager->get($id);
        $response = new ResponseObject();

        if($user == null) {
            $response->withError(new ResponseError(ResponseCode::NOT_FOUND, ResponseMessage::USER_NOT_FOUND));

            return $this->renderResult($response);
        }

        $response->withParams('user', $user->toArray());

        return $this->renderResult($response);
    }

    /**
     * @Route("/user", name="ws_post_user", methods="POST")
     */
    public function postAction(Request $request) {
        $requestParams = $this->getRequestParams($request);
        $notValidatedFields = [];
        $entityManager = new UserEntityManager($this->getDoctrineManager());
        $response = new ResponseObject();

        $user = new User();
        $user->buildFromRequest($requestParams);

        if(!TypeUtils::isEquals($user->getPassword(),$requestParams['password_verify'])){
            $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::PASSWORD_NOT_MATCH));
            return $this->renderResult($response);
        }

        try {
            $notValidatedFields = ValidatorManager::validate($user);
        } catch (ValidatorException $e) {
            $response->withError(new ResponseError(ResponseCode::SERVER_ERROR, ResponseMessage::SERVER_ERROR));

            return $this->renderResult($response);
        }

        if(!empty($notValidatedFields)) {
            $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::BAD_REQUEST));
            $response->withParams('fields', $notValidatedFields);

            return $this->renderResult($response);
        } else {
            try {
                $createdUser = $entityManager->create($user);
                $this->sendRegistrationMail($user);
            } catch(UniqueConstraintViolationException $e) {
                $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::EMAIL_OR_NICKNAME_ALREADY_USE));
                $response->withParams('fields', $notValidatedFields);

                return $this->renderResult($response);
            }
            catch(\Exception $e) {
                $response->withError(new ResponseError(ResponseCode::SERVER_ERROR, ResponseMessage::SERVER_ERROR));

                return $this->renderResult($response);
            }

            $response->withCode(ResponseCode::CREATED);
            $response->withParams('user', $createdUser->toArray());
        }

        return $this->renderResult($response);
    }

    /**
     * @Route("/user/{id}", name="ws_put_user", methods={"PUT", "OPTIONS"})
     */
    public function putAction($id, Request $request) {
        $params = $this->getRequestParams($request);
        $entityManager = new UserEntityManager($this->getDoctrineManager());
        $user = $entityManager->get($id);
        $response = new ResponseObject();

        if($user == null) {
            $response->withError(new ResponseError(ResponseCode::NOT_FOUND, ResponseMessage::USER_NOT_FOUND));

            return $this->renderResult($response);
        }

        if( isset($params['password']) && isset($params['password_verify']) ){
            // Pour la modification d'un password
            if( isset($params['last_password']) ) {
                if (CryptUtils::hashPassword($params['last_password']) != $user->getPassword()
                    || !TypeUtils::isEquals($params['password'], $params['password_verify'])) {
                    $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::MODIFY_PASSWORD));

                    return $this->renderResult($response);
                }

                unset($params['password_verify']);
                unset($params['last_password']);
                $params['password'] = CryptUtils::hashPassword($params['password']);
            }
            // Pour le renouvellement du password
            else{
                if(!isset($params['password']) || !isset($params['password_verify']) ||
                    !TypeUtils::isEquals($params['password'],$params['password_verify'])){
                    $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::MODIFY_PASSWORD));

                    return $this->renderResult($response);
                }

                unset($params['password_verify']);
                $params['password'] = CryptUtils::hashPassword($params['password']);
            }
        }

        try {
            $user->updateWithParams($params);
        } catch (ParameterNotFoundException $e) {
            $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::INVALID_PARAMETER . $e->getKey()));

            return $this->renderResult($response);
        }

        try {
            $notValidatedFields = ValidatorManager::validate($user);
        } catch (ValidatorException $e) {
            $response->withError(new ResponseError(ResponseCode::SERVER_ERROR, ResponseMessage::SERVER_ERROR));

            return $this->renderResult($response);
        }

        if(!empty($notValidatedFields)) {
            $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::BAD_REQUEST));
            $response->withParams('fields', $notValidatedFields);

            return $this->renderResult($response);
        } else {
            try {
                $createdUser = $entityManager->update($id, $user);
            } catch(UniqueConstraintViolationException $e) {
                $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::EMAIL_OR_NICKNAME_ALREADY_USE));
                $response->withParams('fields', $notValidatedFields);

                return $this->renderResult($response);
            }

            $response->withCode(ResponseCode::OK);
            $response->withParams('user', $createdUser->toArray());
        }

        return $this->renderResult($response);
    }

    /**
     * @Route("/user/{id}", name="ws_delete_user", methods="DELETE")
     */
    public function deleteAction($id) {
        $entityManager = new UserEntityManager($this->getDoctrineManager());
        $user = $entityManager->get($id);
        $response = new ResponseObject();

        if($user == null) {
            $response->withError(new ResponseError(ResponseCode::NOT_FOUND, ResponseMessage::USER_NOT_FOUND));

            return $this->renderResult($response);
        }

        $entityManager->delete($id);
        $response->withCode(ResponseCode::NO_CONTENT);

        return $this->renderResult($response);
    }

    /**
     * @Route("/user/auth", name="ws_auth_user", methods="POST")
     */
    public function authAction(Request $request) {
        $entityManager = new UserEntityManager($this->getDoctrineManager());
        $params = $this->getRequestParams($request);
        $response = new ResponseObject();

        if(!array_key_exists('email', $params) || !array_key_exists('password', $params)) {
            $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::INVALID_PARAMETERS));
            return $this->renderResult($response);
        }

        if(!$entityManager->authenticateUser($params['email'], $params['password'])) {
            $response->withCode(ResponseCode::UNAUTHORIZED);
        } else {
            $response->withCode(ResponseCode::OK);
        }

        return $this->renderResult($response);

    }

    /**
     * @Route("/users/search", name="ws_search_user", methods="GET")
     */
    public function searchAction(Request $request) {
        $entityManager = new UserEntityManager($this->getDoctrineManager());
        $params = $this->getQueryParams($request);
        $response = new ResponseObject();

        try {
            $users = $entityManager->search($params);

            foreach($users as $user) {
                $responseParam[] = $user->toArray();
            }

            $response->withCode(ResponseCode::OK);
            $response->withParams('users', $responseParam);
        } catch (InvalidParameterException $e) {
            $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::INVALID_PARAMETERS));
        }

        return $this->renderResult($response);
    }

    /**
     * @Route("/users", name="ws_all_user", methods="GET")
     */
    public function getAllAction() {
        $entityManager = new UserEntityManager($this->getDoctrineManager());
        $response = new ResponseObject();

        $users = $entityManager->getAll();

        foreach($users as $user) {
            $responseParam[] = $user->toArray();
        }

        $response->withCode(ResponseCode::OK);
        $response->withParams('users', $responseParam);

        return $this->renderResult($response);
    }

    private function sendRegistrationMail($user) {
        $mailer = $this->container->get(ServiceUtils::MAILER_SERVICE);
        $mailer->setContainer($this->container);
        $mailer->sendRegistrationMail($user);
    }

    /**
     * @Route("/user/confirm/{token}", name="registration_confirm")
     */
    public function confirmAccountAction($token, Request $request){
        $userManager = new UserEntityManager($this->getDoctrineManager());
        $response = new ResponseObject();

        try {
            $userManager->confirmAccount($token);
        }
        catch(EntityNotFoundException $e){
            $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::USER_NOT_FOUND));

            return $this->renderResult($response);
        }

        $response->withCode(ResponseCode::OK);

        return $this->renderResult($response);
    }

    private function sendLostPasswordMail($user) {
        $mailer = $this->container->get(ServiceUtils::MAILER_SERVICE);
        $mailer->setContainer($this->container);
        $mailer->sendLostPasswordMail($user);
    }

    /**
     * @Route("user/lostpassword", name="send_lost_password", methods="POST")
     */
    public function sendLostPasswordAction(Request $request){
        $userManager = new UserEntityManager($this->getDoctrineManager());
        $params = $this->getRequestParams($request);
        $response = new ResponseObject();

        $user = $userManager->search(array("email" => $params['email']));

        if($user == null || !isset($user[0])){
            $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::USER_NOT_FOUND));

            return $this->renderResult($response);
        }

        $this->sendLostPasswordMail($user[0]);
        $response->withCode(ResponseCode::OK);

        return $this->renderResult($response);
    }

    /**
     * @Route("user/lostpassword/{token}", name="change_lost_password")
     */
    public function lostPasswordAction($token, Request $request){
        $userManager = new UserEntityManager($this->getDoctrineManager());
        $response = new ResponseObject();
        $user = $userManager->search(array("token" => $token));

        if($user == null || !isset($user[0])){
            $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::USER_NOT_FOUND));

            return $this->renderResult($response);
        }

        return $this->putAction($user[0]->getId(), $request);

    }

}