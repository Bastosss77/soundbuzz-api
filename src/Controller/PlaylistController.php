<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 08/06/18
 * Time: 11:44
 */

namespace App\Controller;

use App\common\exception\InvalidParameterException;
use App\common\manager\PlaylistEntityManager;
use App\common\manager\PlaylistTrackEntityManager;
use App\common\manager\UserEntityManager;
use App\common\response\ResponseCode;
use App\common\response\ResponseError;
use App\common\response\ResponseMessage;
use App\common\response\ResponseObject;
use App\common\validator\ValidatorException;
use App\common\validator\ValidatorManager;
use App\Entity\Playlist;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class PlaylistController extends BaseWSController {

    /**
     * @Route("/playlist/{id}", name="ws_get_playlist", methods="GET")
     */
    public function getAction($id) {
        $entityManager = new PlaylistEntityManager($this->getDoctrineManager());
        $playlist = $entityManager->get($id);
        $response = new ResponseObject();

        if($playlist == null) {
            $response->withError(new ResponseError(ResponseCode::NOT_FOUND, ResponseMessage::PLAYLIST_NOT_FOUND));

            return $this->renderResult($response);
        }

        $response->withParams('playlist', $playlist->toArray());

        return $this->renderResult($response);
    }

    /**
     * @Route("/playlist", name="ws_post_playlist", methods="POST")
     */
    public function postAction(Request $request){
        $params = $this->getRequestParams($request);
        $response = new ResponseObject();
        $manager = new PlaylistEntityManager($this->getDoctrineManager());

        $playlist = new Playlist();
        $playlist->buildFromRequest($params);

        try {
            $notValidatedFields = ValidatorManager::validate($playlist);
        } catch (ValidatorException $e) {
            $response->withError(new ResponseError(ResponseCode::SERVER_ERROR, ResponseMessage::SERVER_ERROR));

            return $this->renderResult($response);
        }

        try{
            $manager->uniqueLabelByUser($params["user"],$params["label"]);
        }
        catch (InvalidParameterException $e){
            $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::PLAYLIST_UNIQUE));

            return $this->renderResult($response);
        }

        if(!empty($notValidatedFields)) {
            $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::BAD_REQUEST));
            $response->withParams('fields', $notValidatedFields);

            return $this->renderResult($response);
        } else {
            try {
                $createdPlaylist = $manager->create($playlist);
            }
            catch (EntityNotFoundException $e){
                $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::PLAYLIST_NOT_FOUND));

                return $this->renderResult($response);
            }
            $response->withCode(ResponseCode::CREATED);
            $response->withParams('playlist', $createdPlaylist->toArray());
        }

        return $this->renderResult($response);

    }

    /**
     * @Route("/playlist/{id}", name="ws_put_playlist", methods="PUT")
     */
    public function putAction($id, Request $request) {
        $params = $this->getRequestParams($request);
        $entityManager = new PlaylistEntityManager($this->getDoctrineManager());
        $playlist = $entityManager->get($id);
        $response = new ResponseObject();

        if($playlist == null) {
            $response->withError(new ResponseError(ResponseCode::NOT_FOUND, ResponseMessage::PLAYLIST_NOT_FOUND));

            return $this->renderResult($response);
        }

        try {
            $playlist->updateWithParams($params);
        } catch (ParameterNotFoundException $e) {
            $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::INVALID_PARAMETER . $e->getKey()));

            return $this->renderResult($response);
        }

        try {
            $notValidatedFields = ValidatorManager::validate($playlist);
        } catch (ValidatorException $e) {
            $response->withError(new ResponseError(ResponseCode::SERVER_ERROR, ResponseMessage::SERVER_ERROR));

            return $this->renderResult($response);
        }

        if(!empty($notValidatedFields)) {
            $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::BAD_REQUEST));
            $response->withParams('fields', $notValidatedFields);

            return $this->renderResult($response);
        } else {
            try {
                $createdPlaylist = $entityManager->update($id, $playlist);
            } catch(\Exception $e) {
                $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::BAD_REQUEST));
                $response->withParams('fields', $notValidatedFields);

                return $this->renderResult($response);
            }

            $response->withCode(ResponseCode::OK);
            $response->withParams('playlist', $createdPlaylist->toArray());
        }

        return $this->renderResult($response);
    }

    /**
     * @Route("/playlist/{id}", name="ws_delete_playlist", methods="DELETE")
     */
    public function deleteAction($id) {
        $entityManager = new PlaylistEntityManager($this->getDoctrineManager());
        $playlist = $entityManager->get($id);
        $response = new ResponseObject();

        if($playlist == null) {
            $response->withError(new ResponseError(ResponseCode::NOT_FOUND, ResponseMessage::PLAYLIST_NOT_FOUND));

            return $this->renderResult($response);
        }

        $entityManager->delete($id);
        $response->withCode(ResponseCode::NO_CONTENT);

        return $this->renderResult($response);
    }

    /**
     * @Route("/playlist/add/{id}", name="ws-add_track_playlist", methods="PUT")
     */
    public function addTrack($id, Request $request) {
        $entityManager = new PlaylistEntityManager($this->getDoctrineManager());
        $response = new ResponseObject();
        $params = $this->getRequestParams($request);

        if(!isset($params['trackId']) || intval($params['trackId']) == 0) {
            $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::INVALID_PARAMETER));

            return $this->renderResult($response);
        }

        try{
            $entityManager->addTrack($id, intval($params['trackId']));
        } catch(EntityNotFoundException $e) {
            $response->withError(new ResponseError(ResponseCode::NOT_FOUND, ResponseMessage::PLAYLIST_OR_TRACK_NOT_FOUND));

            return $this->renderResult($response);
        }

        $response->withCode(ResponseCode::OK);

       return $this->renderResult($response);
    }

    /**
     * @Route("/playlists", methods="GET")
     */
    public function getPlaylistForUser(Request $request) {
        $params = $this->getQueryParams($request);
        $response = new ResponseObject();

        if(!isset($params['userId'])) {
            $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::MISSING_USER_ID));

            return $this->renderResult($response);
        }

        $userEntityManager = new UserEntityManager($this->getDoctrineManager());
        $user = $userEntityManager->get($params['userId']);

        if($user == null) {
            $response->withError(new ResponseError(ResponseCode::NOT_FOUND, ResponseMessage::USER_NOT_FOUND));

            return $this->renderResult($response);
        }

        $playlistEntityManager = new PlaylistEntityManager($this->getDoctrineManager());

        try {
            $playlists = $playlistEntityManager->getPlaylistsForUser($user->getId());
        } catch (EntityNotFoundException $e) {
            $response->withError(new ResponseError(ResponseCode::NOT_FOUND, ResponseMessage::USER_NOT_FOUND));

            return $this->renderResult($response);
        }

        $response->withCode(ResponseCode::OK);
        $arrayResponse = array();

        foreach($playlists as $playlist) {
            $arrayResponse[] = $playlist->toArray();
        }

        $response->withParams('playlists', $arrayResponse);

        return $this->renderResult($response);
    }

    /**
     * @Route("/playlist/{id}/tracks", methods="GET")
     */
    public function getTracksForPlaylist($id) {
        $manager = new PlaylistEntityManager($this->getDoctrineManager());
        $response = new ResponseObject();

        try {
            $tracks = $manager->getTracksForPlaylist($id);
        } catch (EntityNotFoundException $e) {
            $response->withError(New ResponseError(ResponseCode::NOT_FOUND, ResponseMessage::PLAYLIST_NOT_FOUND));

            return $this->renderResult($response);
        }

        $responseArray = array();
        foreach ($tracks as $track) {
            $responseArray[] = $track->toArray();
        }

        $response->withCode(ResponseCode::OK);
        $response->withParams('tracks', $responseArray);

        return $this->renderResult($response);
    }
}