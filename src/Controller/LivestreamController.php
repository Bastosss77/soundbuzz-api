<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 08/06/18
 * Time: 15:52
 */

namespace App\Controller;

use App\common\manager\LivestreamEntityManager;
use App\common\response\ResponseCode;
use App\common\response\ResponseError;
use App\common\response\ResponseMessage;
use App\common\response\ResponseObject;
use App\common\validator\ValidatorException;
use App\common\validator\ValidatorManager;
use App\Entity\Livestream;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class LivestreamController extends BaseWSController {

    /**
     * @Route("/livestream/{id}", methods={"GET"})
     */
    public function live($id) {

    }

    /**
     * @Route("/livestreams", methods={"GET"})
     */
    public function livestreams() {
        $entityManager = new LivestreamEntityManager($this->getDoctrineManager());
        $lives = $entityManager->getAll();

        foreach($lives as $live) {
            $responseParam[] = $live->toArray();
        }

        $response = new ResponseObject();
        $response->withCode(ResponseCode::OK)
            ->withParams('livestreams', $responseParam);

        return $this->renderResult($response);
    }

    /**
     * @Route("/livestream/new/{userId}", methods={"GET"})
     */
    public function startNewLive($userId) {
        $entityManager = new LivestreamEntityManager($this->getDoctrineManager());
        $live = $entityManager->getByUserId($userId);
        $response = new ResponseObject();

        if($live != null) {
            $response->withError(new ResponseError(ResponseCode::UNAUTHORIZED, ResponseMessage::ALREADY_STREAMING));
            return $this->renderResult($response);
        }

        $livestream = new Livestream();
        $livestream->setStreamId(uniqid());
        $livestream->setUserId($userId);

        try {
            $fieldsError = ValidatorManager::validate($livestream);
        } catch (ValidatorException $e) {
            $response->withError(new ResponseError(ResponseCode::SERVER_ERROR, ResponseMessage::SERVER_ERROR));
            return $this->renderResult($response);
        }

        if(!empty($notValidatedFields)) {
            $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::BAD_REQUEST));
            $response->withParams('fields', $notValidatedFields);

            return $this->renderResult($response);
        } else {
            try {
                $livestream = $entityManager->create($livestream);
            } catch (EntityNotFoundException $e) {
                $response->withError(new ResponseError(ResponseCode::NOT_FOUND, ResponseMessage::USER_NOT_FOUND));
                return $this->renderResult($response);
            }

            $response->withCode(ResponseCode::CREATED)
                ->withParams('livestream', $livestream->toArray());
        }

        return $this->renderResult($response);
    }
}