<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 08/06/18
 * Time: 11:44
 */

namespace App\Controller;

use App\common\helper\TrackHelper;
use App\common\manager\MusicTypeEntityManager;
use App\common\manager\TrackEntityManager;
use App\common\response\ResponseCode;
use App\common\response\ResponseError;
use App\common\response\ResponseMessage;
use App\common\response\ResponseObject;
use App\common\validator\ValidatorException;
use App\common\validator\ValidatorManager;
use App\Entity\Track;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;
use Symfony\Component\Routing\Annotation\Route;


class TrackController extends BaseWSController {

    /**
     * @Route("/tracks", name="ws_get_tracks", methods="GET")
     */
    public function getAllTracks() {
        $response = new ResponseObject();
        $manager = new TrackEntityManager($this->getDoctrineManager());

        $tracks = [];
        foreach ($manager->getAll() as $track) {
            $tracks[] = $track->toArray();
        }

        $response->withCode(ResponseCode::OK);
        $response->withParams('tracks', $tracks);

        return $this->renderResult($response);
    }

    /**
     * @Route("/track/{id}", name="ws_get_track", methods="GET")
     */
    public function getAction($id) {
        $entityManager = new TrackEntityManager($this->getDoctrineManager());
        $track = $entityManager->get($id);
        $response = new ResponseObject();

        if($track == null) {
            $response->withError(new ResponseError(ResponseCode::NOT_FOUND, ResponseMessage::TRACK_NOT_FOUND));

            return $this->renderResult($response);
        }

        $response->withParams('track', $track->toArray());

        return $this->renderResult($response);
    }

    /**
     * @Route("/track/metadata", name="ws_post_metadata", methods="POST")
     */
    public function readMetadata(Request $request) {
        $response = new ResponseObject();

        try {
            $arrayMetaData = TrackHelper::readMetadata($request->files->get('filename'));
        } catch (UnsupportedMediaTypeHttpException $e) {
            $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::UNSUPPORTED_TYPE));

            return $this->renderResult($response);
        }

        $response->withCode(ResponseCode::OK)
            ->withParams('metadata', $arrayMetaData);

        return $this->renderResult($response);
    }

    /**
     * @Route("/track", name="ws_post_track", methods="POST")
     */
    public function postAction(Request $request){
        $params = $this->getRequestParams($request);
        $entityManager = new TrackEntityManager($this->getDoctrineManager());
        $response = new ResponseObject();

        $track = new Track();
        $track->buildFromRequest($params);

        try {
            $notValidatedFields = ValidatorManager::validate($track);
        } catch (ValidatorException $e) {
            $response->withError(new ResponseError(ResponseCode::SERVER_ERROR, ResponseMessage::SERVER_ERROR));

            return $this->renderResult($response);
        }

        if(!empty($notValidatedFields)) {
            $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::BAD_REQUEST));
            $response->withParams('fields', $notValidatedFields);

            return $this->renderResult($response);
        } else {
            try {
                $createdTrack = $entityManager->create($track);
            } catch(EntityNotFoundException $e) {
                $response->withError(new ResponseError(ResponseCode::NOT_FOUND, ResponseMessage::USER_OR_MUSICTYPE_NOT_FOUND));
                $response->withParams('fields', $notValidatedFields);

                return $this->renderResult($response);
            }

            $response->withCode(ResponseCode::CREATED);
            $response->withParams('track', $createdTrack->toArray());
        }

        return $this->renderResult($response);
    }

    /**
     * @Route("/track/{id}", name="ws_put_track", methods={"PUT", "OPTIONS"})
     */
    public function putAction($id, Request $request) {
        $params = $this->getRequestParams($request);
        $entityManager = new TrackEntityManager($this->getDoctrineManager());
        $track = $entityManager->get($id);
        $response = new ResponseObject();

        if($track == null) {
            $response->withError(new ResponseError(ResponseCode::NOT_FOUND, ResponseMessage::TRACK_NOT_FOUND));

            return $this->renderResult($response);
        }

        try {
            $track->updateWithParams($params);
        } catch (ParameterNotFoundException $e) {
            $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::INVALID_PARAMETER . $e->getKey()));

            return $this->renderResult($response);
        }

        try {
            $notValidatedFields = ValidatorManager::validate($track);
        } catch (ValidatorException $e) {
            $response->withError(new ResponseError(ResponseCode::SERVER_ERROR, ResponseMessage::SERVER_ERROR));

            return $this->renderResult($response);
        }

        if(!empty($notValidatedFields)) {
            $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::BAD_REQUEST));
            $response->withParams('fields', $notValidatedFields);

            return $this->renderResult($response);
        } else {
            try {
                $createdTrack = $entityManager->update($id, $track);
            } catch(\Exception $e) {
                $response->withError(new ResponseError(ResponseCode::BAD_REQUEST, ResponseMessage::BAD_REQUEST));
                $response->withParams('fields', $notValidatedFields);

                return $this->renderResult($response);
            }

            $response->withCode(ResponseCode::OK);
            $response->withParams('track', $createdTrack->toArray());
        }

        return $this->renderResult($response);
    }

    /**
     * @Route("/track/{id}", name="ws_delete_track", methods="DELETE")
     */
    public function deleteAction($id) {
        $entityManager = new TrackEntityManager($this->getDoctrineManager());
        $track = $entityManager->get($id);
        $response = new ResponseObject();

        if($track == null) {
            $response->withError(new ResponseError(ResponseCode::NOT_FOUND, ResponseMessage::TRACK_NOT_FOUND));

            return $this->renderResult($response);
        }

        $entityManager->delete($id);
        $response->withCode(ResponseCode::NO_CONTENT);

        return $this->renderResult($response);
    }

    /**
     * @Route("/tracks/types", name="ws_get_trackType", methods="GET")
     */
    public function getAllTypes() {
        $response = new ResponseObject();
        $manager = new MusicTypeEntityManager($this->getDoctrineManager());

        $types = [];
        foreach ($manager->getAll() as $type) {
            $types[] = $type->toArray();
        }

        $response->withCode(ResponseCode::OK);
        $response->withParams('types', $types);

        return $this->renderResult($response);
    }

    /**
     * @Route("/tracks/play", methods="GET")
     */
    public function play(Request $request) {
        $params = $this->getQueryParams($request);
        $data = TrackHelper::getTrackData($params['name']);
        $response = new Response($data);

        $response->headers->set("Content-type", "text/plain");
        $response->headers->set("Access-Control-Allow-Origin", "*");
        $response->headers->set("Access-Control-Allow-Methods", "GET, OPTIONS");

        return $response;
    }

}