<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 11/06/18
 * Time: 10:45
 */

namespace App\Repository;


use App\Entity\Livestream;

class LivestreamRepository extends BaseRepository {

    protected function getEntityClass() {
        return Livestream::class;
    }
}