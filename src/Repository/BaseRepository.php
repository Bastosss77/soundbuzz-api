<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 04/06/18
 * Time: 15:53
 */

namespace App\Repository;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

abstract class BaseRepository extends ServiceEntityRepository {

    public function __construct(RegistryInterface $registry) {
        parent::__construct($registry, $this->getEntityClass());
    }

    protected abstract function getEntityClass();
}