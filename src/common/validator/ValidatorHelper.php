<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 05/06/18
 * Time: 19:33
 */

namespace App\common\validator;


class ValidatorHelper {

    public static function getRegexFromFieldType(int $type) : string {
        switch($type) {
            case ValidatorManager::TYPE_BOOL;
                return FieldValidator::BOOLEAN_FIELD_REGEX;
            case ValidatorManager::TYPE_INT:
                return FieldValidator::NUMBER_FIELD_REGEX;
                case ValidatorManager::TYPE_STRING;
                return FieldValidator::TEXT_FIELD_REGEX;
            case ValidatorManager::TYPE_EMAIL:
                return FieldValidator::EMAIL_FIELD_REGEX;
            case ValidatorManager::TYPE_PASSWORD:
                return FieldValidator::PASSWORD_REGEX;
            case ValidatorManager::TYPE_STRING_NUMBER:
                return FieldValidator::TEXT_NUMBER_FIELD_REGEX;
            default:
                return "";
        }
    }
}