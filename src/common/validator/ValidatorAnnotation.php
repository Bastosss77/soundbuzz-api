<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 05/06/18
 * Time: 17:11
 */

namespace App\common\validator;


/**
 * Class Validator
 * @package App\common\validator
 *
 * @Annotation
 * @Target("PROPERTY")
 */
class ValidatorAnnotation {
    public $required;
    public $type;
}