<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 05/06/18
 * Time: 17:23
 */

namespace App\common\validator;


use App\common\utils\TypeUtils;
use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\Common\Annotations\AnnotationReader;

class ValidatorManager {

    const TYPE_BOOL = 1;
    const TYPE_INT = 2;
    const TYPE_STRING = 3;
    const TYPE_EMAIL = 4;
    const TYPE_PASSWORD = 5;
    const TYPE_STRING_NUMBER = 6;

    private static $mapType = [
        'boolean' => self::TYPE_BOOL,
        'integer' => self::TYPE_INT,
        'string' => self::TYPE_STRING,
        'email' => self::TYPE_EMAIL,
        'password' => self::TYPE_PASSWORD,
        'text' => self::TYPE_STRING_NUMBER
    ];

    /**
     * @param $object
     * @return array
     * @throws ValidatorException
     */
    public static function validate($object) {
        $errorFields = [];

        try {
            $reader = new AnnotationReader();
            $reflectionClass = new \ReflectionClass(get_class($object));

            foreach ($reflectionClass->getProperties() as $property) {
                if($annotation = $reader->getPropertyAnnotation($property, 'App\\common\\validator\\ValidatorAnnotation')) {
                    $type = $annotation->type;
                    $required = $annotation->required;

                    $property->setAccessible(true);
                    $value = $property->getValue($object);
                    $property->setAccessible(false);

                    if(!is_bool($required)) {
                        throw new ValidatorException(ValidatorException::REQUIRED_MUST_BE_BOOLEAN);
                    }

                    if(!is_string($type) || $type == "" || !array_key_exists($type, self::$mapType)) {
                        throw new ValidatorException(ValidatorException::TYPE_MUST_BE_SPECIFIED);
                    }

                    if (!self::isFieldValid($value, self::$mapType[$type], $required)) {
                        $errorFields[] = $property->getName();
                    }
                }
            }

            return $errorFields;
        } catch (AnnotationException | \ReflectionException $e) {
            throw(new ValidatorException(ValidatorException::CANNOT_VALIDATE));
        }
    }

    private static function isFieldValid($value, int $type, $required): bool {
        if(!$required && $value == null) {
            return true;
        }

        if(is_bool($value) && $type == self::TYPE_BOOL) {
            return true;
        }

        if(is_integer($value) && $type == self::TYPE_INT) {
            return true;
        }

        if($type == self::TYPE_PASSWORD && strlen($value) == 64) {
            return true;
        }

        if($value == null) {
            return false;
        }


        $regex = ValidatorHelper::getRegexFromFieldType($type);

        if($type == self::TYPE_INT && is_object($value)){
            return preg_match($regex, $value->getId()) == 1;
        }

        return preg_match($regex, $value) == 1;
    }
}