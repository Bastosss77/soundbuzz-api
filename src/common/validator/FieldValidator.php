<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 21/07/17
 * Time: 11:26
 */

namespace App\common\validator;


class FieldValidator {
    const TEXT_FIELD_REGEX = '/^[a-zA-ZáéíóúüÁÉÍÓÚÜ\-_\' ]*$/';
    const ADDRESS_FIELD_REGEX = '/^[0-9]{1,3} [a-zA-ZáéíóúüÁÉÍÓÚÜ_\-\' ]*$/';
    const ZIPCODE_FIELD_REGEX = '/^(F-)?((2[A|B])|[0-9]{2})[0-9]{3}$/';
    const NUMBER_FIELD_REGEX = '/^[0-9]*$/';
    const PHONE_FIELD_REGEX = '/^0[0-9]{9}$/';
    const DATE_REGEX = '/^([0-2]\d|3[0-1])\/(0\d|1[0-2])\/(1|2)\d{3}$/';
    const BOOLEAN_FIELD_REGEX = '/(0|1){1}/';
    const EMAIL_FIELD_REGEX = '/^\w+([-+.\']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/';
    const DECIMAL_NUMBER_REGEX = '/^[0-9]*(\.[0-9]{0,2}|)$/';
    const PASSWORD_REGEX = '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/';
    const TEXT_NUMBER_FIELD_REGEX = '/^[a-zA-ZáéíóúüÁÉÍÓÚÜ\-_\' 0-9]*$/';

    public static function isFieldValueValid($val, $regex) {
        return preg_match($regex, $val);
    }
}