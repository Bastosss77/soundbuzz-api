<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 05/06/18
 * Time: 19:44
 */

namespace App\common\validator;


class ValidatorException extends \Exception {

    const REQUIRED_MUST_BE_BOOLEAN = 'Required field must be boolean';
    const TYPE_MUST_BE_SPECIFIED = 'Type field must be specified';
    const VALUE_IS_NULL = 'Field is required but value is null';
    const CANNOT_VALIDATE = 'Cannot validate';
}