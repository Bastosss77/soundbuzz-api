<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 04/06/18
 * Time: 15:28
 */

namespace App\common\response;


class ResponseError {
    private $mCode;
    private $mMessage;

    public function __construct(int $code, string $message) {
        $this->mCode = $code;
        $this->mMessage = $message;
    }

    public function getCode() : int {
        return $this->mCode;
    }

    public function getMessage() : string {
        return $this->mMessage;
    }
}