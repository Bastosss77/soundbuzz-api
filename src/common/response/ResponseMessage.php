<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 05/06/18
 * Time: 19:44
 */

namespace App\common\response;


class ResponseMessage {

    //Server
    const ENDPOINT_NOT_IMPLEMENTED = 'Endpoint not implemented';
    const SERVER_ERROR = 'Something wrong appear';
    const BAD_REQUEST = 'Error on fields';
    const INVALID_PARAMETER = 'Invalid parameter : ';
    const INVALID_PARAMETERS = "Invalid parameters";

    //User
    const USER_NOT_FOUND = 'User not found';
    const EMAIL_OR_NICKNAME_ALREADY_USE = 'Email or nickname already used';
    const MODIFY_PASSWORD = 'Error on passwords';
    const PASSWORD_NOT_MATCH = 'Password not matching with verify password';
    const MISSING_USER_ID = 'Missing user id';

    //Track
    const TRACK_NOT_FOUND = 'Track not found';
    const UNSUPPORTED_TYPE = 'Media type not supported';
    const USER_OR_MUSICTYPE_NOT_FOUND = "User or music type not found";

    //Comment
    const COMMENT_NOT_FOUND = 'Comment not found';

    //Playlist
    const PLAYLIST_NOT_FOUND = 'Playlist not found';
    const PLAYLIST_UNIQUE = 'Playlist must have unique name';
    const PLAYLIST_OR_TRACK_NOT_FOUND = 'Playlist or track not found';

    //Livestream
    const ALREADY_STREAMING = 'User is already streaming';

}