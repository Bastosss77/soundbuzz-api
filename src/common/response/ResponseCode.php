<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 04/06/18
 * Time: 15:23
 */

namespace App\common\response;


abstract class ResponseCode {
    const OK = 200;
    const CREATED = 201;
    const NO_CONTENT = 204;
    const BAD_REQUEST = 400;
    const UNAUTHORIZED = 401;
    const NOT_FOUND = 404;
    const SERVER_ERROR = 500;
    const NOT_IMPLEMENTED = 501;
}