<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 04/06/18
 * Time: 15:11
 */

namespace App\common\response;


class ResponseObject {
    private $mCode = ResponseCode::OK;
    private $mResponseArray;

    public function withCode(int $code) : ResponseObject {
        $this->mCode = $code;

        return $this;
    }

    public function withParams(string $key, $value) : ResponseObject {
        $this->mResponseArray[$key] = $value;

        return $this;
    }

    public function getCode(): int {
        return $this->mCode;
    }

    public function withError(ResponseError $error) : ResponseObject {
        $this->mCode = $error->getCode();
        $this->mResponseArray['error'] = $error->getMessage();

        return $this;
    }

    public function toJson() : string {
        return json_encode(
            [
                "code" => $this->mCode,
                "content" => $this->mResponseArray
            ]);
    }
}