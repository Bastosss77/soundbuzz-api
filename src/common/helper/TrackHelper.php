<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 08/06/18
 * Time: 12:19
 */

namespace App\common\helper;



use App\common\utils\ConstantUtils;
use GetId3\GetId3Core;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;

class TrackHelper {

    public static function readMetadata($file) {

        if(in_array($file->getMimeType(), ConstantUtils::TRACK_MIME_TYPE)){
            $file->move(ConstantUtils::AUDIO_TEMP_DIR, $file->getClientOriginalName());
            $pathFile = ConstantUtils::AUDIO_TEMP_DIR."/".$file->getClientOriginalName();
        }
        else{
            throw new UnsupportedMediaTypeHttpException();
        }

        $getId3 = new GetId3Core();
        $audio = $getId3
            ->setOptionMD5Data(true)
            ->setEncoding('UTF-8')
            ->analyze($pathFile);

        $composer = isset($audio['tags']['id3v2']['composer'][0]) ? $audio['tags']['id3v2']['composer'][0] : null;
        $title = isset($audio['tags']['id3v2']['title'][0]) ? $audio['tags']['id3v2']['title'][0] : null;
        $description = isset($audio['tags']['id3v2']['comment'][0]) ? $audio['tags']['id3v2']['comment'][0] : null;
        $genre = isset($audio['tags']['id3v2']['genre'][0]) ? $audio['tags']['id3v2']['genre'][0] : null;
        // On ne gère qu'un type de musique par morceaux.
        if($genre != null){
            $genre = explode(";", $genre)[0];
        }
        $duration = round($audio['playtime_seconds']) ?? null;
        $photo = isset($audio['comments']['picture'][0]['data']) ? base64_encode($audio['comments']['picture'][0]['data']) : null;

        return array(
            "composer" => $composer,
            "title" => $title,
            "description" => $description,
            "musicType" => $genre,
            "duration" => $duration,
            "photo" => $photo,
            "filename" => $file->getClientOriginalName()
        );
    }

    public static function getTrackData($name) {
        return 'data:audio/mpeg;base64,' .
            base64_encode(file_get_contents($name));
    }

}