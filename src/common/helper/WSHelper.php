<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 08/06/18
 * Time: 12:19
 */

namespace App\common\helper;



use App\common\utils\ConstantUtils;
use GetId3\GetId3Core;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;

class WSHelper {

    public static function generateAccountToken() {
        return bin2hex(random_bytes(16));
    }

}