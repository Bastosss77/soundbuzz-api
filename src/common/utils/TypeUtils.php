<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 07/06/18
 * Time: 15:24
 */

namespace App\common\utils;


use App\Entity\User;

class TypeUtils {

    const DATE_FORMAT = 'd/m/Y';
    const DATETIME_FORMAT = 'd/m/Y H:i:s';

     public static function isEquals($string, $verify) : bool {
        if($string == $verify)
            return true;
        return false;
    }

    public static function dateArrayToString(array $date) {
        $dateString = strlen($date['day']) == 1 ? "0{$date['day']}" : $date['day'];
        $dateString .= '/' . (strlen($date['month']) == 1 ? "0{$date['month']}" : $date['month']);
        $dateString .= "/{$date['year']}";

        return $dateString;
    }

    public static function stringToDateTime(string $date) {
        return \DateTime::createFromFormat(self::DATE_FORMAT, $date);
    }

    public static function saveBase64Data($filename, $base64String) {
        $filepath = ConstantUtils::IMAGE_DIR . '/' . $filename . ConstantUtils::IMAGE_EXTENSION;
        $ifp = fopen($filepath, 'w');

        fwrite($ifp, base64_decode($base64String));
        fclose($ifp);
    }
}