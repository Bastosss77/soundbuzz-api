<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 21/07/17
 * Time: 10:28
 */

namespace App\common\utils;


class ArrayUtils {

    public static function isSafetyField(string $field, array $array) {
        return (isset($array[$field]) && ($array[$field] != null || $array[$field] != ''));
    }

    public static function isSafetyEmpty($array) {
        return $array == null || empty($array);
    }

}