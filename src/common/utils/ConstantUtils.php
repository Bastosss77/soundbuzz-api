<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 23/08/17
 * Time: 11:26
 */

namespace App\common\utils;


class ConstantUtils {
    const ROOT = __DIR__ . '/../../..';

    const IMAGE_MIME_TYPE = array('image/jpeg','image/png');
    const TRACK_MIME_TYPE = array('audio/mpeg', 'audio/ogg', 'audio/flac', 'audio/x-flac', 'audio/x-wav');

    const AUDIO_TEMP_DIR = self::ROOT . '/audio-dir';
    const AUDIO_IMG_DEFAULT = 'default-track.png';

    const IMAGE_DIR = self::ROOT . '/img-dir';
    const IMAGE_EXTENSION = '.png';

    //Mail
    const SWIFT_MAILER_USER = 'mailer_user';
    const MAIL_REGISTRATION_SUBJECT = 'Inscription sur notre site';
    const MAIL_REGISTRATION_BODY = 'emailRegistration.twig';
    const MAIL_LOST_PASSWORD_SUBJECT = 'Mot de passe perdu !';
    const MAIL_LOST_PASSWORD_BODY = 'emailLostPassword.twig';

}