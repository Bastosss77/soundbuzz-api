<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 07/06/18
 * Time: 10:12
 */

namespace App\common\utils;


class CryptUtils {
    private const PASSWORD_ALGO = 'sha256';

    public static function hashPassword($password): string {
        return hash(self::PASSWORD_ALGO, $password);
    }

}