<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 07/06/18
 * Time: 14:38
 */

namespace App\common\manager;


use App\Entity\Comment;
use Doctrine\ORM\EntityNotFoundException;

class CommentEntityManager extends BaseEntityManager {

    protected function getRepositoryName(){
        return Comment::class;
    }

    public function create($object) {
        $userManager = new UserEntityManager($this->mEntityManager);
        $trackManager = new TrackEntityManager($this->mEntityManager);

        $user = $userManager->get($object->getUser());
        $track = $trackManager->get($object->getTrack());

        if($user == null || $track == null){
            throw new EntityNotFoundException();
        }

        $object->setUser($user);
        $object->setTrack($track);

        return parent::create($object);
    }

    public function update($id, $params) {
        //Cannot update
    }

    public function delete($id) {
        //Cannot delete
    }
}