<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 06/06/18
 * Time: 13:51
 */

namespace App\common\manager;


use App\common\utils\CryptUtils;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

abstract class BaseEntityManager {
    protected $mEntityManager;
    protected $mRepository;

    public function __construct(ObjectManager $objectManager) {
        $this->mEntityManager = $objectManager;
        $this->mRepository = $objectManager->getRepository($this->getRepositoryName());
    }

    protected abstract function getRepositoryName();

    protected function getManager(): ObjectManager {
        return $this->mEntityManager;
    }

    public function get($id) {
        return $this->mRepository->find($id);
    }

    public function create($object) {

        $this->mEntityManager->persist($object);
        $this->mEntityManager->flush();

        return $object;
    }

    public function update($id, $params) {
        $object = $this->get($id);
        $object->updateWithParams($params);

        if(array_key_exists('password', $params)) {
            $object->setPassword(CryptUtils::hashPassword($object->getPassword()));
        }

        $this->mEntityManager->persist($object);
        $this->mEntityManager->flush();

        return $this->get($id);
    }

    public function delete($id){
        $object = $this->get($id);

        $this->mEntityManager->remove($object);
        $this->mEntityManager->flush();
    }

}