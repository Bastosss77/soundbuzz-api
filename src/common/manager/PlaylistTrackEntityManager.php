<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 07/06/18
 * Time: 14:38
 */

namespace App\common\manager;


use App\Entity\PlaylistTrack;
use Doctrine\ORM\EntityNotFoundException;

class PlaylistTrackEntityManager extends BaseEntityManager {

    protected function getRepositoryName(){
        return PlaylistTrack::class;
    }

    public function getTracksForPlaylist($playlist) {
        return $this->mRepository->findBy(['playlist' => $playlist]);
    }
}