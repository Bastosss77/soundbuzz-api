<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 07/06/18
 * Time: 14:38
 */

namespace App\common\manager;


use App\common\exception\InvalidParameterException;
use App\Entity\Playlist;
use App\Entity\PlaylistTrack;
use Doctrine\ORM\EntityNotFoundException;

class PlaylistEntityManager extends BaseEntityManager {

    protected function getRepositoryName(){
        return Playlist::class;
    }

    public function create($object) {
        $userManager = new UserEntityManager($this->mEntityManager);
        $user = $userManager->get($object->getUser());

        if($user == null){
            throw new EntityNotFoundException();
        }
        $object->setUser($user);

        return parent::create($object);
    }

    public function update($id, $params) {
        return parent::update($id,$params);
    }

    public function delete($id) {
        parent::delete($id);
    }

    public function uniqueLabelByUser($user, $label){
        $uniq_playlist = $this->mRepository->findBy(["user" => $user, "label" => $label]);

        if($uniq_playlist != null)
            throw new InvalidParameterException();
    }

    /**
     * @param $playlistId
     * @param $trackId
     * @throws EntityNotFoundException
     */
    public function addTrack($playlistId, $trackId) {
        $playlistTrackManager = new PlaylistTrackEntityManager($this->mEntityManager);
        $playlist = $this->get($playlistId);
        $track = (new TrackEntityManager($this->mEntityManager))->get($trackId);
        $uniqTrackPlaylist = $playlistTrackManager->mRepository->findBy(["playlist" => $playlistId, "track" => $trackId]);

        if($playlist == null || $track == null) {
            throw new EntityNotFoundException();
        }

        if(!empty($uniqTrackPlaylist)){
            throw new InvalidParameterException();
        }

        $playlistTrackEntityManager = new PlaylistTrackEntityManager($this->mEntityManager);
        $playlistTrack = new PlaylistTrack();
        $playlistTrack->setPlaylist($playlist);
        $playlistTrack->setTrack($track);
        $playlistTrack->setSequence($this->calSeq($playlistId));

        $playlistTrackEntityManager->create($playlistTrack);
    }

    /**
     * @param $userId
     * @return array
     * @throws EntityNotFoundException
     */
    public function getPlaylistsForUser($userId) {
        $userManager = new UserEntityManager($this->mEntityManager);
        $user = $userManager->get($userId);

        if($user == null) {
            throw new EntityNotFoundException();
        }

        return $this->mRepository->findBy(['user' => $user]);
    }

    /**
     * @param $playlistId
     * @throws EntityNotFoundException
     */
    public function getTracksForPlaylist($playlistId) {
        $playlist = $this->get($playlistId);

        if($playlist == null) {
            throw new EntityNotFoundException();
        }

        $playlistTrackManager = new PlaylistTrackEntityManager($this->mEntityManager);
        $playlistTracks = $playlistTrackManager->getTracksForPlaylist($playlist);
        $trackEntityManager = new TrackEntityManager($this->mEntityManager);
        $tracks = array();

        foreach($playlistTracks as $playlistTrack) {
            $tracks[] = $trackEntityManager->get($playlistTrack->getId());
        }

        return $tracks;
    }

    private function calSeq($playlistId){
        $playlistTrackManager = new PlaylistTrackEntityManager($this->mEntityManager);
        $playlistTracks = $playlistTrackManager->getTracksForPlaylist($playlistId);

        return (count($playlistTracks)+1);
    }
}