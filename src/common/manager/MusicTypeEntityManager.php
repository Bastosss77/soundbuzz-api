<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 07/06/18
 * Time: 14:38
 */

namespace App\common\manager;


use App\Entity\MusicType;

class MusicTypeEntityManager extends BaseEntityManager {

    protected function getRepositoryName(){
        return MusicType::class;
    }

    public function create($object) {
        //Cannot create
    }

    public function update($id, $params) {
        //Cannot update
    }

    public function delete($id) {
        //Cannot delete
    }

    public function getAll() {
        return $this->mRepository->findAll();
    }
}