<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 06/06/18
 * Time: 15:11
 */

namespace App\common\manager;


use App\common\exception\InvalidParameterException;
use App\common\utils\CryptUtils;
use App\Entity\Role;
use App\Entity\User;
use Doctrine\ORM\EntityNotFoundException;

class UserEntityManager extends BaseEntityManager {

    protected function getRepositoryName() {
        return User::class;
    }

    public function create($object){
        $hashedPassword = CryptUtils::hashPassword($object->getPassword());
        $roleEntityManager = new RoleEntityManager($this->getManager());

        $object->setPassword($hashedPassword);

        $object->setRole($roleEntityManager->get(Role::NORMAL_USER));

        return parent::create($object);
    }

    public function authenticateUser($email, $password): bool {
        $hashPassword = CryptUtils::hashPassword($password);

        $user = $this->mRepository->findOneBy(['email' => $email,
            'password' => $hashPassword,
            'isactif' => 1]);

        return $user != null;
    }

    /**
     * @param array $params
     * @return array
     * @throws InvalidParameterException
     */
    public function search(array $params) {
        try {
            $search =  $this->mRepository->findBy($params);

            return $search ?? [];
        } catch (\Exception $e) {
            throw new InvalidParameterException();
        }
    }

    public function getAll() {
        return $this->mRepository->findAll() ?? [];
    }

    public function delete($id){
        $object = $this->get($id);
        $object->setIsactif(false);

        $this->mEntityManager->persist($object);
        $this->mEntityManager->flush();
    }

    public function confirmAccount($token) {

        $user = $this->mRepository->findBy(array("token" => $token))[0];

        if($user == null) {
            throw new EntityNotFoundException();
        }

        $user->setIsactif(true);

        $this->mEntityManager->persist($user);
        $this->mEntityManager->flush();

    }


}