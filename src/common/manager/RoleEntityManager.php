<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 07/06/18
 * Time: 14:38
 */

namespace App\common\manager;


use App\Entity\Role;

class RoleEntityManager extends BaseEntityManager {

    protected function getRepositoryName(){
        return Role::class;
    }

    public function create($object) {
        //Cannot create
    }

    public function update($id, $params) {
        //Cannot update
    }

    public function delete($id) {
        //Cannot delete
    }
}