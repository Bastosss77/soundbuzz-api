<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 08/06/18
 * Time: 12:01
 */

namespace App\common\manager;

use App\common\utils\ConstantUtils;
use App\common\utils\TypeUtils;
use App\Entity\Track;
use Doctrine\ORM\EntityNotFoundException;

class TrackEntityManager extends BaseEntityManager {

    protected function getRepositoryName() {
        return Track::class;
    }

    /**
     * @param $object
     * @return mixed
     * @throws EntityNotFoundException
     */
    public function create($object) {
        $userManager = new UserEntityManager($this->mEntityManager);
        $musicTypeManager = new MusicTypeEntityManager($this->mEntityManager);
        $user = $userManager->get($object->getUser());
        $musicType = $musicTypeManager->get($object->getMusicType());

        TypeUtils::saveBase64Data($object->getFilename(), $object->getPhoto());
        $object->setPhoto($object->getFilename());

        if($user == null || $musicType == null){
            throw new EntityNotFoundException();
        }

        $object->setUser($user);
        $object->setMusicType($musicType);

        return parent::create($object);
    }

    public function delete($id){
        $object = $this->get($id);
        $this->mEntityManager->remove($object);
        $this->mEntityManager->flush();

        unlink(ConstantUtils::AUDIO_TEMP_DIR . '/' . $object->getFileName());
        unlink(ConstantUtils::IMAGE_DIR. '/' . $object->getPhoto());
    }

    public function getAll() {
        return $this->mRepository->findAll();
    }
}