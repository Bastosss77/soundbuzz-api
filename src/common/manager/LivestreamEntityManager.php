<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 11/06/18
 * Time: 10:57
 */

namespace App\common\manager;


use App\Entity\Livestream;
use Doctrine\ORM\EntityNotFoundException;

class LivestreamEntityManager extends BaseEntityManager {

    protected function getRepositoryName() {
        return Livestream::class;
    }

    public function getByUserId($userId) {
        return $this->mRepository->findOneBy(['user_id' => $userId]);
    }

    /**
     * @param $object
     * @throws EntityNotFoundException
     */
    public function create($object) {
        $userManager = new UserEntityManager($this->mEntityManager);
        $user = $userManager->get($object->getUserId());

        if($user == null) {
            throw new EntityNotFoundException();
        }

        $object->setUserId($user);

        return parent::create($object);
    }

    public function getAll() {
        return $this->mRepository->findAll();
    }

}