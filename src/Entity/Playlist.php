<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlaylistRepository")
 */
class Playlist extends BaseEntity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlaylistTrack", mappedBy="playlist", cascade={"remove"}, orphanRemoval=true)
     */
    private $playlistTrack;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    public function __construct()
    {
        $this->playlistTrack = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    protected function getEntityName()
    {
        return Playlist::class;
    }

    public function buildFromRequest(array $params)
    {
        $this->label = $params['label'];
        $this->description = $params['description'];
        $this->user = $params['user'];
    }

    public function toArray(){
        $array = array(
            "id" => $this->id,
            "label" => $this->label,
            "description" => $this->description,
            "user" => $this->user->toArray()
        );

        return $array;
    }

    /**
     * @return Collection|PlaylistTrack[]
     */
    public function getPlaylistTrack(): Collection
    {
        return $this->playlistTrack;
    }

    public function addPlaylistTrack(PlaylistTrack $playlistTrack): self
    {
        if (!$this->playlistTrack->contains($playlistTrack)) {
            $this->playlistTrack[] = $playlistTrack;
            $playlistTrack->setPlaylist($this);
        }

        return $this;
    }

    public function removePlaylistTrack(PlaylistTrack $playlistTrack): self
    {
        if ($this->playlistTrack->contains($playlistTrack)) {
            $this->playlistTrack->removeElement($playlistTrack);
            // set the owning side to null (unless already changed)
            if ($playlistTrack->getPlaylist() === $this) {
                $playlistTrack->setPlaylist(null);
            }
        }

        return $this;
    }

}
