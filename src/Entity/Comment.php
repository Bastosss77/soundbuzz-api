<?php

namespace App\Entity;

use App\common\validator\ValidatorAnnotation;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommentRepository")
 */
class Comment extends BaseEntity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Track")
     * @ORM\JoinColumn(name="track_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $track;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @ValidatorAnnotation(type="string", required=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="boolean")
     */
    private $signaled = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getTrack()
    {
        return $this->track;
    }

    public function setTrack(?Track $track): self
    {
        $this->track = $track;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getSignaled()
    {
        return $this->signaled;
    }

    public function setSignaled(bool $signaled)
    {
        $this->signaled = $signaled;
    }

    protected function getEntityName()
    {
        return Comment::class;
    }

    public function buildFromRequest(array $params)
    {
        $this->user = $params['user'];
        $this->track = $params['track'];
        $this->comment = $params['comment'];
    }

    public function toArray()
    {
        return array(
            "id" => $this->id,
            "user" => $this->user->toArray(),
            "track" => $this->track->toArray(),
            "comment" => $this->comment,
            "signaled" => $this->signaled
        );
    }
}
