<?php

namespace App\Entity;

use App\common\helper\WSHelper;
use Doctrine\ORM\Mapping as ORM;
use App\common\validator\ValidatorAnnotation;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User extends BaseEntity {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Role")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id", nullable=false)
     */
    private $role;

    /**
     * @ORM\Column(type="string", length=100)
     * @ValidatorAnnotation(type="string", required=true)
     */ 
    private $firstname;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     * @ValidatorAnnotation(type="string", required=true)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=200, unique=true)
     * @ValidatorAnnotation(type="email", required=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=200, unique=false)
     * @ValidatorAnnotation(type="password", required=true)
     */
    private $password;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     * @ValidatorAnnotation(type="integer", required=false)
     */
    private $age;

    /**
     * @ORM\Column(type="string", length=200, nullable=false, unique=true)
     * @ValidatorAnnotation(type="text", required=true)
     */
    private $artist_name;

    /**
     * @ORM\Column(type="boolean")
     * @ValidatorAnnotation(type="boolean", required=false)
     */
    private $isactif = false;

    /**
     * @ORM\Column(type="string")
     */
    private $token;

    public function getId()
    {
        return $this->id;
    }

    public function getRole(): ?Role
    {
        return $this->role;
    }

    public function setRole(?Role $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(?int $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getArtistName(): ?string
    {
        return $this->artist_name;
    }

    public function setArtistName(?string $artist_name): self
    {
        $this->artist_name = $artist_name;

        return $this;
    }

    public function getIsactif(): ?bool
    {
        return $this->isactif;
    }

    public function setIsactif(bool $isactif): self
    {
        $this->isactif = $isactif;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    protected function getEntityName() {
        return User::class;
    }

    public function buildFromRequest(array $params){
        $this->firstname = $params['firstname'] ?? null;
        $this->lastname = $params['lastname'] ?? null;
        $this->email = $params['email'] ?? null;
        $this->password = $params['password'] ?? null;
        $this->age = $params['age'] ?? 0;
        $this->artist_name = $params['artist_name'] ?? $this->firstname . ' ' . $this->lastname;
        $this->token = WSHelper::generateAccountToken();
    }

    public function toArray()
    {
        return array(
          "id" => $this->id,
          "firstname" => $this->firstname,
          "lastname" => $this->lastname,
          "email" => $this->email,
          "age" => $this->age,
          "artist_name" => $this->artist_name,
          "role" => $this->role->toArray(),
          "isactif" => $this->isactif,
          "token" => $this->token
        );
    }
}
