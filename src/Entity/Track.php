<?php

namespace App\Entity;

use App\common\utils\ConstantUtils;
use App\common\utils\TypeUtils;
use App\common\validator\ValidatorAnnotation;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TrackRepository")
 */
class Track extends BaseEntity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * @ValidatorAnnotation(type="integer", required=true)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MusicType")
     * @ORM\JoinColumn(name="musicType_id", referencedColumnName="id", nullable=false)
     * @ValidatorAnnotation(type="integer", required=true)
     */
    private $musicType;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     * @ValidatorAnnotation(type="string", required=false)
     */
    private $composer;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $filename;

    /**
     * @ORM\Column(type="boolean")
     * @ValidatorAnnotation(type="boolean", required=false)
     */
    private $isdownloadable;

    /**
     * @ORM\Column(type="string", length=150)
     * @ValidatorAnnotation(type="string", required=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @ValidatorAnnotation(type="string", required=false)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * @ORM\Column(type="boolean")
     */
    private $explicit_content;

    /**
     * @ORM\Column(type="integer")
     */
    private $duration;

    /**
     * @ORM\Column(type="datetime")
     */
    private $transfer_dt;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $visible = false;

    /**
     * @ORM\Column(type="integer")
     */
    private $nblike = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbread = 0;

    /**
     * @ORM\Column(type="boolean")
     * @ValidatorAnnotation(type="boolean", required=false)
     */
    private $signaled = false;

    public function __construct()
    {
        $this->playlist = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getComposer(): ?string
    {
        return $this->composer;
    }

    public function setComposer(?string $composer): self
    {
        $this->composer = $composer;

        return $this;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getIsdownloadable(): ?bool
    {
        return $this->isdownloadable;
    }

    public function setIsdownloadable(bool $isdownloadable): self
    {
        $this->isdownloadable = $isdownloadable;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPhoto()
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getExplicitContent(): ?bool
    {
        return $this->explicit_content;
    }

    public function setExplicitContent(bool $explicit_content): self
    {
        $this->explicit_content = $explicit_content;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getTransferDt(): ?\DateTime
    {
        return $this->transfer_dt;
    }

    public function setTransferDt(\DateTime $transfer_dt): self
    {
        $this->transfer_dt = $transfer_dt;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getMusicType()
    {
        return $this->musicType;
    }

    public function setMusicType(?MusicType $musicType): self
    {
        $this->musicType = $musicType;

        return $this;
    }

    public function getNblike(): ?int
    {
        return $this->nblike;
    }

    public function setNblike(int $nblike): self
    {
        $this->nblike = $nblike;

        return $this;
    }

    public function getNbread(): ?int
    {
        return $this->nbread;
    }

    public function setNbread(int $nbread): self
    {
        $this->nbread = $nbread;

        return $this;
    }

    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    public function setVisible(bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }

    public function getSignaled()
    {
        return $this->signaled;
    }

    public function setSignaled(bool $signaled)
    {
        $this->signaled = $signaled;
    }

    protected function getEntityName() {
        return Track::class;
    }

    public function buildFromRequest(array $params){
        $this->composer = $params['composer'] ?? null;
        $this->isdownloadable = $params['isdownloadable'] ?? true;
        $this->title = $params['title'] ?? null;
        $this->explicit_content = $params['explicit_content'] ?? false;
        $this->duration = $params['duration'] ?? null;
        $this->transfer_dt = isset($params['transfer_dt']) ? TypeUtils::stringToDateTime($params['transfer_dt']) : new \DateTime();
        $this->nblike = $params['nblike'] ?? 0;
        $this->nbread = $params['nbread'] ?? 0;
        $this->visible = $params['visible'] ?? true;
        $this->user = $params['user'] ?? null;
        $this->musicType = $params['musicType'] ?? null;
        $this->filename = $params['filename'] ?? null;
        $this->photo = $params['photo'] ?? ConstantUtils::AUDIO_IMG_DEFAULT;
    }

    public function toArray()
    {

        $imagedata = file_get_contents(ConstantUtils::IMAGE_DIR . '/' . $this->photo);
        $base64Image = base64_encode($imagedata);


        return array(
            "id" => $this->id,
            "composer" => $this->composer,
            "isdownloadable" => $this->isdownloadable,
            "title" => $this->title,
            "explicit_content" => $this->explicit_content,
            "duration" => $this->duration,
            "signaled" => $this->signaled,
            "transfer_dt" => $this->transfer_dt,
            "nblike" => $this->nblike,
            "nbread" => $this->nbread,
            "visible" => $this->visible,
            "filename" => ConstantUtils::AUDIO_TEMP_DIR. '/'. $this->filename,
            "photo" => $base64Image,
            "user" => $this->user->toArray(),
            "musicType" => $this->musicType->toArray()
        );
    }

}
