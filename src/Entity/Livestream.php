<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 11/06/18
 * Time: 10:43
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\common\validator\ValidatorAnnotation;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LivestreamRepository")
 */
class Livestream extends BaseEntity {

    protected function getEntityName() {
        return LiveStream::class;
    }

    public function buildFromRequest(array $params) {
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * @ValidatorAnnotation(type="integer", required=true)
     */
    private $user_id;

    /**
     * @ORM\Column(type="string")
     */
    private $streamId;


    public function getId()
    {
        return $this->id;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function setUserId($id): self
    {
        $this->user_id = $id;

        return $this;
    }

    public function getStreamId(): ?string {
        return $this->streamId;
    }

    public function setStreamId(string $streamId) {
        $this->streamId = $streamId;

        return $this;
    }

    public function toArray()
    {
        return array(
            "id" => $this->id,
            "user" => $this->user_id->toArray(),
            "streamId" => $this->streamId
        );
    }
}