<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlaylistTrackRepository")
 */
class PlaylistTrack extends BaseEntity
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="App\Entity\Track")
    * @ORM\JoinColumn(name="track_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
    */
    private $track;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Playlist", inversedBy="playlistTrack")
     * @ORM\JoinColumn(name="playlist_id", referencedColumnName="id", nullable=false)
     */
    private $playlist;

    /**
     * @ORM\Column(type="integer")
     */
    private $sequence;

    public function getId()
    {
        return $this->id;
    }

    public function getSequence(): ?int
    {
        return $this->sequence;
    }

    public function setSequence(int $sequence): self
    {
        $this->sequence = $sequence;

        return $this;
    }

    public function getTrack(): ?Track
    {
        return $this->track;
    }

    public function setTrack(?Track $track): self
    {
        $this->track = $track;

        return $this;
    }

    public function getPlaylist(): ?Playlist
    {
        return $this->playlist;
    }

    public function setPlaylist(?Playlist $playlist): self
    {
        $this->playlist = $playlist;

        return $this;
    }

    protected function getEntityName()
    {
        return PlaylistTrack::class;
    }

    public function buildFromRequest(array $params)
    {

    }

    public function toArray()
    {
        return array(
            "id" => $this->id,
            "sequence" => $this->sequence,
            "playlist" => $this->playlist->toArray(),
            "track" => $this->track->toArray()
        );
    }

}
