<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RoleRepository")
 */
class Role extends BaseEntity {
    const NORMAL_USER = 1;
    const MODERATOR = 2;
    const ADMINISTRATOR = 3;

    protected function getEntityName() {
        return Role::class;
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $label;

    public function getId()
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function buildFromRequest(array $params){
    }

    public function toArray()
    {
        return array(
            "id" => $this->id,
            "label" => $this->label
        );
    }
}
