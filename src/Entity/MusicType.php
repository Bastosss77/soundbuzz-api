<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MusicTypeRepository")
 */
class MusicType extends BaseEntity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    public function getId()
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function buildFromRequest(array $params)
    {
        // TODO: Implement buildFromRequest() method.
    }

    protected function getEntityName()
    {
        return MusicType::class;
    }

    public function toArray(){
        return array(
            "id" => $this->id,
            "label" => $this->label
        );
    }
}
