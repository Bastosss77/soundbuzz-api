<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 05/06/18
 * Time: 15:11
 */

namespace App\Entity;


use App\common\utils\TypeUtils;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;

abstract class BaseEntity {

    public abstract function buildFromRequest(array $params);
    protected abstract function getEntityName();

    public function toJson() {
        return json_encode($this->toArray());
    }

    public function toArray() {
        try {
            $values = [];
            $properties = (new \ReflectionClass($this))->getProperties();

            foreach ($properties as $property) {
                if($property->isPrivate()) {
                    $property->setAccessible(true);
                }
                if($property->getValue($this) instanceof \DateTime){
                    $values[$property->getName()] = $property->getValue($this)->format(TypeUtils::DATETIME_FORMAT);
                } elseif(is_object($property->getValue($this))) {
                    $values[$property->getName()] = $property->getValue($this)->toArray();
                } else {
                    $values[$property->getName()] = $property->getValue($this);
                }
            }

            return $values;
        } catch(\ReflectionException $e) {
            return [];
        }
    }

    public function updateWithParams($params) {
        try {
            $reflectionClass = new \ReflectionClass($this);

            foreach ($params as $key => $param) {
                $property = $reflectionClass->getProperty($key);

                $property->setAccessible(true);

                $currentValue = $property->getValue($this);

                if(is_bool($currentValue)) {
                    $param = boolval($param);
                }

                $property->setValue($this, $param);
                $property->setAccessible(false);
            }
        } catch (\ReflectionException $e) {
            throw new ParameterNotFoundException($key);
        }
    }
}