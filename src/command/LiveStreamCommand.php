<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 08/06/18
 * Time: 15:42
 */

namespace App\command;


use App\ratchet\LiveStreamServer;
use Ratchet\App;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LiveStreamCommand extends Command {

    protected function configure(){
        $this->setName('soundbuzz:livestream:start')
            ->setDescription('Start livestream server')
            ->addArgument('address', InputArgument::REQUIRED, 'Web Socket server address')
            ->addArgument('port', InputArgument::REQUIRED, 'Web Socket server port');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $output->writeln('Starting WebSocket Server on ' . $input->getArgument('address') . ':' . $input->getArgument('port'));

        $server = new HttpServer(new WsServer(new LiveStreamServer($output)));

        $server = IoServer::factory($server, 7777, '127.0.0.1');
        $server->run();
    }

}