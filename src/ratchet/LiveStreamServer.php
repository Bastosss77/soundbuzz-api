<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 08/06/18
 * Time: 15:31
 */

namespace App\ratchet;


use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LiveStreamServer implements MessageComponentInterface {

    private $mLivestreams;
    private $mOutput;

    public function __construct(OutputInterface $output) {
        $this->mLivestreams = new \SplObjectStorage();
        $this->mOutput = $output;
    }

    public function onOpen(ConnectionInterface $conn) {
        $conn->send(json_encode(['action' => 'connected', 'data' => $conn->resourceId]));
    }

    public function onClose(ConnectionInterface $conn) {
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        $messageData = json_decode($msg);

        if($messageData == null) {
            return false;
        }

        $action = $messageData->action ?? 'unknown';
        $liveId = $messageData->liveId;

        switch ($action) {
            case 'server_create':
                return $this->createNewStreamSession($liveId, $from, $messageData->userId);
                case 'server_close';
                return $this->closeStreamSession($liveId);
            case 'server_candidate':
                return $this->livestreamServerCandidates($liveId, $messageData->candidate);
            case 'server_description':
                return $this->livestreamServerDescription($liveId, $messageData->description);
            case 'client_log':
                return $this->addClientToStream($liveId, $from, $messageData->username);
            case 'client_logout':
                return $this->clientLogOut($liveId, $from->resourceId);
            case 'client_candidate':
                return $this->livestreamClientCandidates($liveId, $from->resourceId, $messageData->candidate);
            case 'client_description':
                return $this->livestreamClientDescription($liveId, $from->resourceId, $messageData->description);
            case 'client_ask_description':
                return $this->clientAskDescription($liveId, $from);
            case 'client_ask_candidate':
                return $this->clientAskCandidate($liveId, $from);
            default:
                return false;
        }
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        $conn->send('An error has occurred: '.$e->getMessage());
        $conn->close();
    }


    /**********************
     * onMessage functions
     **********************/

    private function createNewStreamSession($liveId, $conn, $userId) {
        $newLivestream = new Livestream($liveId, $userId, $conn);
        $this->mLivestreams->attach($newLivestream);
        $this->mOutput->writeln("New livestream (" . $liveId . ") started by user " . $userId);
        return true;
    }

    private function addClientToStream($liveId, $conn, $username) {
        $client = new LivestreamClient($conn, $username);

        $stream = $this->findStreamByLiveId($liveId);

        if($stream == null) {
            return false;
        }

        if($stream->addClient($client)) {
            $this->mOutput->writeln("New client " . $username . ' on stream ' . $liveId);
            return true;
        } else {
            return false;
        }
    }

    private function closeStreamSession($liveId) {
        $stream = $this->findStreamByLiveId($liveId);

        if($stream == null) {
            return false;
        }

        $stream->close();
        $this->mOutput->writeln('Livestream ' . $stream->getId() . ' has been closed');
        $this->mLivestreams->detach($stream);
        return true;
    }

    private function livestreamServerCandidates($liveId, $candidate) {
        $stream = $this->findStreamByLiveId($liveId);

        if($stream == null) {
            return false;
        }

        $stream->setRTCCandidate($candidate);
        $this->mOutput->writeln('Livestream ' . $stream->getId() . ' set candidate');
        return true;
    }

    private function livestreamServerDescription($liveId, $description) {
        $stream = $this->findStreamByLiveId($liveId);

        if($stream == null) {
            return false;
        }

        $stream->setRTCDescription($description);
        $this->mOutput->writeln('Livestream ' . $stream->getId() . ' set description');
        return true;
    }

    private function clientAskDescription($liveId, $conn) {
        $stream = $this->findStreamByLiveId($liveId);

        if($stream == null) {
            return false;
        }



        $conn->send(json_encode(['action' => 'client_ask_description', 'data' => $stream->getRTCDescription()]));
        $this->mOutput->writeln($conn->resourceId . ' ask for ' . $stream->getId() . ' description');
        return true;
    }

    private function clientAskCandidate($liveId, $conn) {
        $stream = $this->findStreamByLiveId($liveId);

        if($stream == null) {
            return false;
        }

        $conn->send(json_encode(['action' => 'client_ask_candidate', 'data' => $stream->getRTCCandidate()]));
        $this->mOutput->writeln($conn->resourceId . ' ask for ' . $liveId . ' candidate');
        return true;
    }

    private function clientLogout($liveId, $connId) {
        $stream = $this->findStreamByLiveId($liveId);

        if($stream == null) {
            return false;
        }

        $this->mOutput->writeln('Client ' . $connId . ' logged out from ' . $liveId);
        return $stream->clientLoggedOut($connId);
    }

    private function livestreamClientCandidates($liveId, $clientConnId, $candidate) {
        $stream = $this->findStreamByLiveId($liveId);

        if($stream == null) {
            return false;
        }

        $client = $stream->findClientByConnId($clientConnId);
        if($client == null) {
            return false;
        }

        $client->setCandidate($candidate);
        $stream->getOwnerConn()->send(json_encode(['action' => 'client_candidate', 'data' => $client->getCandidate()]));
        $this->mOutput->writeln('Client ' . $clientConnId . ' send candidate for livestream ' . $liveId);

        return true;
    }

    private function livestreamClientDescription($liveId, $clientConnId, $description) {
        $stream = $this->findStreamByLiveId($liveId);

        if($stream == null) {
            return false;
        }

        $client = $stream->findClientByConnId($clientConnId);

        if($client == null) {
            return false;
        }

        $client->setDescription($description);
        $stream->getOwnerConn()->send(json_encode(['action' => 'client_description', 'data' => $client->getDescription()]));
        $this->mOutput->writeln('Client ' . $clientConnId . ' send description for livestream ' . $liveId);

        return true;
    }

    /********************
     * Utils functions
     ********************/
    private function findStreamByLiveId($liveId) {
        foreach($this->mLivestreams as $livestream) {
            if($livestream->getId() === $liveId) {
                return $livestream;
            }
        }

        return null;
    }
}