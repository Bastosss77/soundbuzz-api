<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 11/06/18
 * Time: 15:52
 */

namespace App\ratchet;


class Livestream {
    private $mId;
    private $mOwnerId;
    private $mOwnerConn;
    private $mClients;
    private $mRTCCandidate;
    private $mRTCDescription;

    public function __construct($id, $ownerId, $ownerConn) {
        $this->mId = $id;
        $this->mOwnerId = $ownerId;
        $this->mOwnerConn = $ownerConn;
        $this->mClients = new \SplObjectStorage();
    }

    public function addClient(LivestreamClient $client) {
        foreach($this->mClients as $clientRegistered) {

            if($clientRegistered->getUsername() === $client->getUsername()) {
                return false;
            }
        }

        $this->mClients->attach($client);

        return true;
    }

    public function getId() {
        return $this->mId;
    }

    public function getRTCCandidate() {
        return $this->mRTCCandidate;
    }

    public function setRTCCandidate($candidate) {
        $this->mRTCCandidate = $candidate;
    }

    public function getRTCDescription() {
        return $this->mRTCDescription;
    }

    public function setRTCDescription($description) {
        $this->mRTCDescription = $description;
    }

    public function getOwnerConn() {
        return $this->mOwnerConn;
    }

    public function close() {
        foreach($this->mClients as $client) {
            $client->getConn()->send(json_encode(['action' => 'client_close', 'liveId' => $this->mId]));
        }

        $this->mClients = null;
    }

    public function clientLoggedOut($connId) {
        $client = $this->findClientByConnId($connId);

        if($client == null) {
            return false;
        }

        $client->getConn()->send('You\'ve been disconnected');
        $this->mClients->detach($client);

        foreach($this->mClients as $client) {
            $client->getConn()->send($client->getUsername() . ' has been disconnected');
        }

        return true;
    }

    private function findClientByUsername($username) {
        foreach($this->mClients as $client) {
            if($client->getUsername() === $username) {
                return $client;
            }
        }

        return null;
    }

    public function findClientByConnId($id) {
        foreach($this->mClients as $client) {
            if($client->getConnId() === $id) {
                return $client;
            }
        }

        return null;
    }
}