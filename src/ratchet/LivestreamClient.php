<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 11/06/18
 * Time: 15:56
 */

namespace App\ratchet;


class LivestreamClient {
    private $mConn;
    private $mUsername;
    private $mCandidate;
    private $mDescription;

    public function __construct($conn, $username) {
        $this->mConn = $conn;
        $this->mUsername = $username;
    }

    public function getUsername() {
        return $this->mUsername;
    }

    public function getConn() {
        return $this->mConn;
    }

    public function getConnId() {
        return $this->mConn->resourceId;
    }

    public function setCandidate($candidate) {
        $this->mCandidate = $candidate;
    }

    public function getCandidate() {
        return $this->mCandidate;
    }

    public function setDescription($description) {
        $this->mDescription = $description;
    }

    public function getDescription() {
        return $this->mDescription;
    }
}