<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 07/06/18
 * Time: 14:21
 */

namespace App\DataFixtures;


use App\Entity\MusicType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class MusicTypeFixtures extends Fixture {

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager) {
        foreach ($this->arrayMusicType as $key => $value) {
            $musicType = new MusicType();
            $musicType->setLabel($value);
            $manager->persist($musicType);
        }

        $manager->flush();
    }

    private $arrayMusicType = [
        'Blues',
        'Classic',
        'Country',
        'Dance',
        'Disco',
        'Funk',
        'Grunge',
        'Hip-Hop',
        'Jazz',
        'Metal',
        'New Age',
        'Oldies',
        'Autre',
        'Pop',
        'RnB',
        'Rap',
        'Reggae',
        'Rock',
        'Techno',
        'Musique industrielle',
        'Rock alternatif',
        'Ska',
        'Death Metal',
        'Pranks',
        'Musique de film',
        'Euro techno',
        'Ambient',
        'Trip hop',
        'Musique vocale',
        'Jazz-funk',
        'Fusion',
        'Trance',
        'Musique classique',
        'Instrumental',
        'Acid',
        'House',
        'Musique de jeu vidéo',
        'Extrait sonore',
        'Gospel',
        'Musique bruitiste',
        'Rock alternatif',
        'Bass',
        'Soul',
        'Punk',
        'Space',
        'Musique de relaxation et de méditation',
        'Pop instrumental',
        'Rock instrumental',
        'Musique ethnique',
        'Gothique',
        'Dark wave',
        'Techno-industrial',
        'Musique électronique',
        'Pop folk',
        'Eurodance',
        'Dream',
        'Rock sudiste',
        'Comédie',
        'Morceau culte',
        'Gangsta',
        'Hit-parade',
        'Rap chrétien',
        'Pop/Funk',
        'Jungle',
        'Musique amérindienne',
        'Cabaret',
        'New wave',
        'Psychédélique',
        'Rave',
        'Comédie musicale',
        'Bande-annonce',
        'Lo-fi',
        'Musique tribale',
        'Acid punk',
        'Polka',
        'Rétro',
        'Théâtre',
        'Rock\'n\'Roll',
        'Hard rock',
        'Folk',
        'Folk rock',
        'Folk américain',
        'Swing',
        'Fast fusion',
        'bebop',
        'Musique latine',
        'Revival',
        'Musique celtique',
        'Bluegrass',
        'Avantgarde',
        'Rock gothique',
        'Rock progressif',
        'Rock psychédélique',
        'Rock symphonique',
        'Slow rock',
        'Big bang',
        'Choeur',
        'Easy listening',
        'Acoustique',
        'Humour',
        'Discours',
        'Chanson',
        'Opéra',
        'Musique de chambre',
        'Sonate',
        'Symphonie',
        'Body bass',
        'Primus',
        'Porn groove',
        'Satire',
        'Slow jam',
        'Club',
        'Tango',
        'Samba',
        'Folklore',
        'ballade',
        'Power ballad',
        'Rythmic soull',
        'Duo',
        'Punk rock',
        'Drum solo',
        'A cappella',
        'Euro-house',
        'Dancehall'];
}