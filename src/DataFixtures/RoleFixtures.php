<?php

namespace App\DataFixtures;


use App\Entity\MusicType;
use App\Entity\Role;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class RoleFixtures extends Fixture {

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager) {
        $role1 = new Role();
        $role1->setLabel("Utilisateur");

        $role2 = new Role();
        $role2->setLabel("Modérateur");

        $role3 = new Role();
        $role3->setLabel("Administrateur");

        $manager->persist($role1);
        $manager->persist($role2);
        $manager->persist($role3);

        $manager->flush();
    }
}