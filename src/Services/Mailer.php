<?php

namespace App\Services;

use App\common\utils\ConstantUtils;
use App\common\utils\ServiceUtils;
use App\Entity\User;
use Psr\Container\ContainerInterface;

class Mailer
{
    private $mMailer;
    private $mTwig;
    private $mContainer;

    public function __construct(\Twig_Environment $twig) {
        $this->mTwig = $twig;
    }

    public function setContainer(ContainerInterface $container) {
        $this->mContainer = $container;
        $this->mMailer = $container->get(ServiceUtils::SWITF_MAILER_SERVICE);
    }

    public function sendMail($subject, $email, $template, array $params) {
        if($this->mContainer == null) {
            throw new \Exception("Service container must be set");
        }

        $message = (new \Swift_Message($subject))
            ->setFrom($this->mContainer->getParameter(ConstantUtils::SWIFT_MAILER_USER))
            ->setTo($email)
            ->setBody($this->generateBodyTemplate($template, $params), 'text/html');

       $this->mMailer->send($message);
    }

    private function generateBodyTemplate($template, $params) {
        return $this->mTwig->loadTemplate($template)
            ->renderBlock('body_html', $params);
    }

    public function sendRegistrationMail(User $user) {
        $this->sendMail(
            ConstantUtils::MAIL_REGISTRATION_SUBJECT,
            $user->getEmail(),
            ConstantUtils::MAIL_REGISTRATION_BODY,
            array('name' => $user->getFirstname(), 'token' => $user->getToken()));
    }

    public function sendLostPasswordMail(User $user){
        $this->sendMail(
            ConstantUtils::MAIL_LOST_PASSWORD_SUBJECT,
            $user->getEmail(),
            ConstantUtils::MAIL_LOST_PASSWORD_BODY,
            array('name' => $user->getFirstname(), 'token' => $user->getToken()));
    }

}