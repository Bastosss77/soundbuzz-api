<?php

namespace App\Tests;

use App\common\utils\CryptUtils;
use PHPUnit\Framework\TestCase;

class CryptUtilsTest extends TestCase
{

    public function testHashValid(){
        $hashedPwd = "37d18bc8d68c46de2b0fefeb04dcd27c83996e9b232b874082b8ec10b0a6b0be"; // correspond à Azerty456
        $crypt = CryptUtils::hashPassword("Azerty456");

        $this->assertEquals($crypt,$hashedPwd);
    }

    public function testHashWrong(){
        $hashedPwd = "37d18bc8d68c46de2b0fefeb04dcd27c83996e9b232b874082b8ec10b0a6b0be"; // correspond à Azerty456
        $crypt = CryptUtils::hashPassword("Azerty123");

       $this->assertNotEquals($crypt,$hashedPwd);
    }

}