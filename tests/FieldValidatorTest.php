<?php

namespace App\Tests;


use App\common\validator\FieldValidator;
use PHPUnit\Framework\TestCase;

class FieldValidatorTest extends TestCase
{
    public function testTextValid(){
        $this->assertEquals(true,
            FieldValidator::isFieldValueValid("Coucou ce ceci est un texte", FieldValidator::TEXT_FIELD_REGEX));
    }

    public function testTextWrong(){
        $this->assertEquals(false,
            FieldValidator::isFieldValueValid("061549891", FieldValidator::TEXT_FIELD_REGEX));
    }

    public function testEmailValid(){
        $this->assertEquals(true,
            FieldValidator::isFieldValueValid("email-blabla12@email.fr", FieldValidator::EMAIL_FIELD_REGEX));

    }

    public function testEmailWrong(){
        $this->assertEquals(false,
            FieldValidator::isFieldValueValid("Toto", FieldValidator::EMAIL_FIELD_REGEX));

    }

    public function testPwdValid(){
        $this->assertEquals(true,
            FieldValidator::isFieldValueValid("Azerty123", FieldValidator::PASSWORD_REGEX));

    }

    public function testPwdWrong(){
        $this->assertEquals(false,
            FieldValidator::isFieldValueValid("Azerty1", FieldValidator::PASSWORD_REGEX));

    }

}