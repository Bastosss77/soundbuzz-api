<?php

namespace App\Tests;


use App\common\utils\ConstantUtils;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\File\File;

class TrackTest extends TestCase
{
    private $audioValid;
    private $imageValid;

    protected function setUp()
    {
        $this->audioValid = new File(__DIR__ . "/file/Oxmo_Puccino.mp3");
        $this->imageValid = new File(__DIR__ . "/file/default-track.png");
        parent::setUp();
    }

    protected function tearDown()
    {
        unset($audioValid);
        unset($audioWrong);
        parent::tearDown();
    }

    public function testAudioMimeTypeValid(){
        $resultat = in_array($this->audioValid->getMimeType(),ConstantUtils::TRACK_MIME_TYPE);

        $this->assertEquals(true,$resultat);
    }

    public function testAudioMimeTypeWrong(){
        $resultat = in_array($this->imageValid->getMimeType(),ConstantUtils::TRACK_MIME_TYPE);

        $this->assertEquals(false,$resultat);
    }

    public function testImageMimeTypeValid(){
        $resultat = in_array($this->imageValid->getMimeType(),ConstantUtils::IMAGE_MIME_TYPE);

        $this->assertEquals(true,$resultat);
    }

    public function testImageMimeTypeWrong(){
        $resultat = in_array($this->audioValid->getMimeType(),ConstantUtils::IMAGE_MIME_TYPE);

        $this->assertEquals(false,$resultat);
    }



}