<?php

namespace App\Tests;


use App\common\utils\TypeUtils;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraints\DateTime;

class TypeUtilsTest extends TestCase
{

    public function testIsEqualsValid(){
        $string = "Ca marche";
        $verify = "Ca marche";

        $this->assertEquals(true, TypeUtils::isEquals($string,$verify));
    }

    public function testIsEqualsWrong(){
        $string = "Ca marche";
        $verify = "Ca marche pas";

        $this->assertEquals(false, TypeUtils::isEquals($string,$verify));
    }

    public function testDateArrayToStringValid(){
        $date = array(
            "day" => 12,
            "month" => 4,
            "year" => 1998
        );

        $this->assertEquals("12/04/1998", TypeUtils::dateArrayToString($date));
    }

    public function testDateArrayToStringWrong(){
        $date = array(
            "day" => 12,
            "month" => 6,
            "year" => 1998
        );

        $this->assertNotEquals(TypeUtils::dateArrayToString($date),"12/04/1998");
    }

}